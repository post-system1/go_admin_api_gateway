package handlers

import (
	"context"

	"github.com/gin-gonic/gin"

	"user_interest/ui_go_api_gateway/api/http"
	"user_interest/ui_go_api_gateway/config"
	"user_interest/ui_go_api_gateway/genproto/user_service"
	"user_interest/ui_go_api_gateway/pkg/util"
)

// CreateMarket godoc
// @ID create_market
// @Router /market [POST]
// @Summary Create Market
// @Description  Create Market
// @Tags Market
// @Accept json
// @Produce json
// @Param profile body user_service.CreateMarket true "CreateMarketRequestBody"
// @Success 200 {object} http.Response{data=user_service.Market} "GetMarketBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) CreateMarket(c *gin.Context) {

	var market user_service.CreateMarket

	err := c.ShouldBindJSON(&market)
	if err != nil {
		h.handleResponse(c, http.BadRequest, err.Error())
		return
	}

	resp, err := h.services.MarketService().Create(
		c.Request.Context(),
		&market,
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.Created, resp)
}

// GetMarketByID godoc
// @ID get_market_by_id
// @Router /market/{id} [GET]
// @Summary Get Market  By ID
// @Description Get Market  By ID
// @Tags Market
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=user_service.Market} "MarketBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetMarketByID(c *gin.Context) {

	marketID := c.Param("id")

	if !util.IsValidUUID(marketID) {
		h.handleResponse(c, http.InvalidArgument, "market id is an invalid uuid")
		return
	}

	resp, err := h.services.MarketService().GetByID(
		context.Background(),
		&user_service.MarketPrimaryKey{
			Id: marketID,
		},
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}

// @Security ApiKeyAuth
// GetMarketList godoc
// @ID get_market_list
// @Router /market [GET]
// @Summary Get Market s List
// @Description  Get Market s List
// @Tags Market
// @Accept json
// @Produce json
// @Param offset query integer false "offset"
// @Param limit query integer false "limit"
// @Param search query string false "search"
// @Param Platform-Id header string true "Platform-Id" default(a1924766-a9ee-11ed-afa1-0242ac120001)
// @Success 200 {object} http.Response{data=user_service.GetListMarketResponse} "GetAllMarketResponseBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetMarketList(c *gin.Context) {

	if c.GetHeader("role_id") == config.RoleClient {
		h.handleResponse(c, http.OK, struct{}{})
		return
	}

	offset, err := h.getOffsetParam(c)
	if err != nil {
		h.handleResponse(c, http.InvalidArgument, err.Error())
		return
	}

	limit, err := h.getLimitParam(c)
	if err != nil {
		h.handleResponse(c, http.InvalidArgument, err.Error())
		return
	}

	resp, err := h.services.MarketService().GetList(
		context.Background(),
		&user_service.GetListMarketRequest{
			Limit:  int64(limit),
			Offset: int64(offset),
			Search: c.Query("search"),
		},
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}

// UpdateMarket godoc
// @ID update_market
// @Router /market/{id} [PUT]
// @Summary Update Market
// @Description Update Market
// @Tags Market
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Param profile body user_service.UpdateMarket true "UpdateMarketRequestBody"
// @Success 200 {object} http.Response{data=user_service.Market} "Market data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) UpdateMarket(c *gin.Context) {

	var market user_service.UpdateMarket

	market.Id = c.Param("id")

	if !util.IsValidUUID(market.Id) {
		h.handleResponse(c, http.InvalidArgument, "market id is an invalid uuid")
		return
	}

	err := c.ShouldBindJSON(&market)
	if err != nil {
		h.handleResponse(c, http.BadRequest, err.Error())
		return
	}

	resp, err := h.services.MarketService().Update(
		c.Request.Context(),
		&market,
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}

// DeleteMarket godoc
// @ID delete_market
// @Router /market/{id} [DELETE]
// @Summary Delete Market
// @Description Delete Market
// @Tags Market
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=object{}} "Market data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) DeleteMarket(c *gin.Context) {

	marketId := c.Param("id")

	if !util.IsValidUUID(marketId) {
		h.handleResponse(c, http.InvalidArgument, "market id is an invalid uuid")
		return
	}

	resp, err := h.services.MarketService().Delete(
		c.Request.Context(),
		&user_service.MarketPrimaryKey{Id: marketId},
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.NoContent, resp)
}
