package services

import (
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"

	"user_interest/ui_go_api_gateway/config"
	"user_interest/ui_go_api_gateway/genproto/user_service"
)

type ServiceManagerI interface {
	UserService() user_service.UserServiceClient
	MarketService() user_service.MarketServiceClient
	BranchService() user_service.BranchServiceClient
}

type grpcClients struct {
	userService   user_service.UserServiceClient
	marketService user_service.MarketServiceClient
	branchService user_service.BranchServiceClient
}

func NewGrpcClients(cfg config.Config) (ServiceManagerI, error) {

	// User Service...
	connUserService, err := grpc.Dial(
		cfg.UserServiceHost+cfg.UserGRPCPort,
		grpc.WithTransportCredentials(insecure.NewCredentials()),
	)
	if err != nil {
		return nil, err
	}

	return &grpcClients{
		userService:   user_service.NewUserServiceClient(connUserService),
		marketService: user_service.NewMarketServiceClient(connUserService),
		branchService: user_service.NewBranchServiceClient(connUserService),
	}, nil
}

func (g *grpcClients) UserService() user_service.UserServiceClient {
	return g.userService
}

func (g *grpcClients) MarketService() user_service.MarketServiceClient {
	return g.marketService
}

func (g *grpcClients) BranchService() user_service.BranchServiceClient {
	return g.branchService
}
